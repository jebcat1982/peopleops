1. [ ] People Ops: Once the termination has taken place (whether voluntary or involuntary), as soon as possible, create a **confidential** issue called 'Offboarding (NAME), per (DATE)' in in the [People Ops Issue Tracker](https://gitlab.com/gitlab-com/peopleops/issues) with relevant lines of the master offboarding checklist and /cc @amatthews, @brittanyr.

#### For GitLab BV Belgium only

1. [ ] People Ops: Once the termination date is known verify with Senior People Operations Director or CEO that the non-competition clause is to be waived or enforced.
1. [ ] Non-competition waived: People Ops: send a letter (in French or Dutch depending on location in Belgium) via registered mail and by email to the team member within 15 days of the termination date.
1. [ ] Non-competition enforced: People Ops: to inform Financial Controller & instruct payroll to pay the team member a lump sum as stated in the contract

#### All GitLabbers

1. [ ] For this offboarding, the manager is @MENTION, People Ops is handled by @MENTION.
1. [ ] Google account
   1. [ ] People Ops: Switch off 2FA for the account, reset the password, and add the new credentials to the People Ops vault in 1Password.
   1. [ ] People Ops: Change the phone number and alternative email (typically personal email address) that are associated with the account.
   1. [ ] People Ops: Check with the former team member's manager if they want the former team member's email forwarded to them. If yes, then login to the team member's account and set email to forward to the manager. Note: The account cannot be suspended if emails need to be forwarded. If no, then suspend the account.
   1. [ ] People Ops: Add an automatic response template to the account: ```Hello, {Team Member} has transitioned out of GitLab. If you have a general inquiry please email {Name} at {email}. Thank you, GitLab.```
   1. [ ] People Ops: Switch 2FA back on. Use a Yubikey to turn 2FA back on. 
   1. [ ] People Ops: upon manager's request (typically 4 weeks after blocking
     the Google account), transfer owned documents from Google Drive to
   manager, and delete the Google account. (automatically disables Drive access)
1. [ ] People Ops: Review all open merge requests and reassign to the manager.
1. [ ] People Ops: Remove former GitLabbers' GitLab.com account from the [gitlab-com group](https://gitlab.com/groups/gitlab-com/group_members). Add the handle as a comment in this issue. 
   1. [ ] @rspeicher : Block former GitLabber, remove from all company Groups and Projects, and then unblock. Make sure to change their associated email address to their personal email address.
   1. [ ] @rspeicher : Remove former GitLabber's admin account, if applicable.
1. [ ] People Ops: remove former team member's email from email aliases (workflow is by suggesting in "GitLab Email Forwarding", then enacting).
1. [ ] People Ops: Remove former GitLabbers' GitLab.com account from the [gitlab-org group](https://gitlab.com/groups/gitlab-org/group_members)
1. [ ] People Ops: Block former GitLabbers' [dev.GitLab.org account](https://dev.gitlab.org/admin/users) and remove from [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members).
1. [ ] Slack
   1. [ ] People Ops: check if the team member has created any Slack bots before disabling account 
   1. [ ] People Ops: disable team member in [Slack](https://gitlab.slack.com/admin).
1. [ ] People Ops Information
   1. [ ] People Ops: Remove former team member from TriNet / Savvy and depending on the former team member's location, inform co-employer payrolls. For UK employees instruct payroll to send the individuals P45 to their home address.
   2. [ ] People Ops: For UK employees, remove them from the medical insurance effective from termination date (email Vistra)
   1. [ ] People Ops: Mark former team member as terminated in BambooHR. Use the date mentioned in this issue as final date of employment / contract.
   1. [ ] People Ops: Notify eShares administrator (CFO) of offboarding.
1. [ ] Calendars & Agenda
   1. [ ] People Ops: Remove team member from GitLab availability calendar
   1. [ ] People Ops: Remove team member's birthday and work anniversary from the GitLab Birthdays calendar. Also, un-share the calendar with the team member
   1. [ ] People Ops: Remove team member from the monthly AMA call.
   1. [ ] People Ops: Remove team member from Team Call Agenda and re-arrange the days on which GitLabbers speak in the Team Call Agenda.
   1. [ ] People Ops: If applicable, remove the team member from the APAC team call.
   1. [ ] Manager: Add entry to Team Call agenda to announce departure of team member: 'X is no longer with GitLab'.
   1. [ ] PeopleOps: Remove former team member from the next Retro meeting and the Kickoff meeting that's scheduled and save for all events in the future.
   1. [ ] Manager: Remove team member from team meeting invitations.
   1. [ ] Manager: Cancel weekly 1:1 meetings with new team member.
1. [ ] People Ops: Remove team member from [team page](https://about.gitlab.com/team). Don't forget to remove the profile picture.
1. [ ] People Ops: If there is one, remove team member's pet from [team pets page](https://about.gitlab.com/team-pets). Don't forget to remove the picture.
1. [ ] People Ops: Remove mentions of the team member from our documentation and handbook by doing a text search for their `username` and `first_name` in the `www-gitlab-com` and `gitlab-ce` repositories
and removing them from everywhere except blog posts. If you do not have a tool of choice, try "Find in Project" using Atom.
1. [ ] People Ops: Remove team member from phishing testing platform
1. [ ] People Ops: Reach out to former team member to identify and retrieve any company supplies/equipment. See the [Offboarding page](https://about.gitlab.com/handbook/offboarding/) for further details on that process.
   1. [ ] People Ops: Inform Controller / Accounting if any items in former GitLabbers possession will not be returning, so that they can be removed from asset tracking.
1. [ ] Manager: Announce in #general chat channel as soon as the chat and Google Accounts are blocked: 'As of today, X is no longer with GitLab. Out of respect for their privacy I
can't go into details. If you have questions about tasks or projects that need to be picked up please let me know. If the team member previously made an announcement about leaving, copy the link to the message/mention in the Team Agenda, to just remind everyone that the team member has now left.
It is very important to send this message as soon as possible so people know that they can rely on official communication channels and not have to find out through the grapevine. Delays in announcing it are not acceptable. The policy of not commenting on circumstances is in force indefinitely, even if the termination is voluntary. It is [unpleasant](https://about.gitlab.com/handbook/people-operations/#departures-are-unpleasant), but it is the right thing to do. If people press for answers say you don't want to suggest that underperformance was a reason for this exit but remind them that:
    * It is stated in our general guidelines that job feedback is [between an individual and their manager](https://about.gitlab.com/handbook/general-guidelines/#not-public)
    * If managers do their job right any exit should come as a surprise to everyone except the individual and the manager.
1. [ ] Manager: Organize smooth hand over of any work or tasks from former team member. Offer option to send a message to everyone in the company (i.e. forwarded by the manager), for a farewell message and/or to transmit their personal email address in case people wish to stay in touch.
1. [ ] People Ops: remove former team member from the ["1Password Shared Folders"](https://docs.google.com/a/gitlab.com/spreadsheets/d/1dSlIRbD1b8XisIBbANDK5NGe55LvVSTsWM7aHcWCOVU/edit?usp=sharing) Google Sheet.

### FROM ONBOARDING'S "ON YOUR FIRST DAY"

#### For GitLab Inc employees only

(TriNet Passport=>My Workplace=> Add New Hire/Rehire). This will generate the
welcome email to the employee at their work email on their first date of hire.

#### For GitLab BV employees only

This info is needed to get the team member's profile ready with Savvy HR in order to get pay slips and other information. People Ops will file in BambooHR.

#### All GitLabbers

1. [ ] 1Password
   1. [ ] People Ops: Remove access to 1Password; take a screenshot of the user's permissions and post it in this offboarding issue.
   1. [ ] People Ops: coordinate or actively change sensitive shared passwords. In particular:
      1. [ ] People Ops: Ping admins of department systems to see if the individuals had access.
      1. [ ] sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, discuss others)
      1. [ ] review what vaults former team member had access to, and discuss with vault "owners" which passwords should be changed.
1. [ ] Twitter/TweetDeck
   1. [ ] People Ops: Remove team member from the GitLab twitter group (check with marketing).
   1. [ ] People Ops: Remove access from Tweetdeck for [at]GitHostIO (if applicable).
   1. [ ] People Ops: Remove access from Tweetdeck for [at]gitlabstatus (if applicable).
   1. [ ] People Ops: Remove access from Tweetdeck for [at]GitLabSupport (if applicable).

## FROM ONBOARDING'S "WITHIN FIRST WEEK OF STARTING"

1. [ ] People Ops: Remove from Beamy
1. [ ] People Ops: [Remove team member](https://about.gitlab.com/handbook/people-operations/#add-expensify) from Expensify (if employee).
1. [ ] People Ops: Remove team member from the info sheets of the next Summit if applicable.
1. [ ] People Ops: Remove GitLabbers profile from [Egencia](https://about.gitlab.com/handbook/people-operations/#add-egencia).
1. [ ] PeopleOps: Remove team member from the [public map](https://sundial.teleport.org/public/groups/Y3IahPR5vYjBpMyU2kZj) of everyone's location.You may need to open a New Incognito Window in your browser.

### FOR ENGINEERING ONLY (Devs, PEs, SEs)

1. [ ] Manager: Remove former GitLabbers' GitHub.com account from the [gitlabhq organization](https://github.com/orgs/gitlabhq/people) (if applicable)
1. [ ] Manager: Remove former GitLabbers' account from [Sentry](https://sentry.gitlap.com/organizations/gitlab/members/)
1. [ ] For former Developers (those who had access to part of the infrastructure), and Production GitLabbers: copy offboarding process from [infrastructure](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/doc/offboarding.md) for offboarding action.
1. [ ] Manager: Remove access to PagerDuty if applicable.
1. [ ] Manager (For Build Engineers): Remove team member as a member to the GitLab Dev Digital Ocean account https://cloud.digitalocean.com/settings/team
1. [ ] People Ops: Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).

#### FOR SUPPORT ENGINEERING ONLY

1. [ ] Manager: Remove access to hackerone.com
1. [ ] Manager: Remove access from Tweetdeck for [at]gitlabstatus.
1. [ ] Manager: Remove access from Tweetdeck for [at]githostio.
1. [ ] Zendesk [(general information about removing agents)](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2):
   1. [ ] Manager: Remove any triggers related to the agent - https://gitlab.zendesk.com/agent/admin/triggers
   1. [ ] Manager: Downgrade the agent role to "end-user" - [more information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2)
        + **Warning: This will unassign all tickets from the agent** Consider reducing the "full agent" count on our Zendesk license.
   1. [ ] Manager: Schedule a date to suspend the agents account. [More information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#3)
1. [ ] Community Forum:
   1. [ ] Manager: Remove team member from "moderators" group on the [GitLab community forum](https://forum.gitlab.com/).
1. [ ] Manager: Remove team member as a member from the GitLab Dev DigitalOcean account https://cloud.digitalocean.com/settings/team
1. [ ] People Ops: remove team member's Zoom account.
1. [ ] Manager: Remove team member from https://timezone.io/team/gitlab-support
1. [ ] People Ops: Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).
1. [ ] Hiring Manager: Downgrade GitHost.io account to user privileges - [Set `user_type` to `0`](https://dev.gitlab.org/gitlab/GitHost#create-a-new-admin-user) 

#### FOR UX DESIGNERS, FRONTEND DEVS, AND DESIGNERS ONLY

1. [ ] People Ops: cancel [SketchApp](http://www.sketchapp.com/) license (?).
1. [ ] People Ops: (for Designers only, not UX Designers) remove access to [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html) using the shared credential in the Secretarial vault.
1. [ ] Manager: (for UX Designers) Remove former team member's `Master` access to the [gitlab-design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: (for UX Designers) Remove former team member from the [GitLab Dribbble team](https://dribbble.com/gitlab).
1. [ ] People Ops: (for UX Designers) remove team member from the `@uxers` User Group on Slack.

### FOR MARKETING ONLY

1. [ ] Erica: Remove from Tweetdeck.

### FOR SALES AND FINANCE ONLY

1. [ ] Finance: Remove from Comerica (as user or viewer only if in Finance)
1. [ ] Finance: Remove from [QuickBooks users](https://about.gitlab.com/handbook/hiring/) (finance only)
1. [ ] Manager: Remove from sales meeting.
1. [ ] People Ops: remove team member's Zoom account.
1. [ ] Manager: Remove from  [Salesforce]
1. [ ] Rubén or Oswaldo: Remove from admin panel in the [Subscription portal](https://customers.gitlab.com/admin)

### FOR PEOPLE OPS ONLY

* [ ] People Ops: Remove team member from BambooHR and Workable as an admin.
* [ ] Manager: Remove team member from TriNet and HR Savvy as an admin.

### FOR CORE TEAM MEMBERS ONLY

1.  [ ] People Ops: Remove e-mail address to the mailing list: https://groups.google.com/forum/#!forum/gitlab-core
1.  [ ] People Ops: Remove member to #core in Slack.
1.  [ ] People Ops: Remove member developer access to [gitlab-org](https://gitlab.com/groups/gitlab-org).
1.  [ ] People Ops: Make inactive in BambooHR
