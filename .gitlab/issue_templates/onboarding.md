### BEFORE STARTING AT GITLAB

1. [ ] People Ops (Admin): Once the contract is signed, as soon as possible, create a **confidential** issue called 'Onboarding (NAME), starting (DATE), as (ROLE)' in the [People Ops issue tracker](https://gitlab.com/gitlab-com/peopleops/issues) with relevant lines of the master checklist, paste the private email address of the hire in there and /cc @brittanyr and @chloemw, and the GitLab.com handle of the new team member if known.
1. [ ] Manager is (FILL IN WITH @ HANDLE), buddy is (FILL IN WITH @HANDLE (Nominate someone preferably in similar time zone but different functional group, try to pair engineering and non-engineering if possible)), and People Ops is tackled by (FILL IN WITH @ HANDLE).
1. [ ] People Ops (Admin): Send an email to the chosen buddy letting them know that they have been chosen and include a link to the onboarding issue.
1. [ ] Buddy: Schedule video call for first day to introduce yourself, and make sure they know that if they have questions, they can come to you. Check with the new team member if they will need help setting up their SSH key and installing a Shell ready for using Git locally. You may need to set up an additional meeting for this step. But your main job as a buddy will be to direct them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group on chat, etc.
1. [ ] Google account
   1. [ ] People Ops (Admin): [Create Google account](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx), `first initial last name[at]gitlab.com`, email instructions to private email address, comment with private email below this issue.
   1. [ ] People Ops (Admin): Add new Google user to ['New Users' group](https://admin.google.com/gitlab.com/AdminHome?groupId=new-users@gitlab.com&chromeless=1#OGX:Group) to disable 2FA for them
   1. [ ] New team member: Google does not support 2FA in some countries ([e.g. Nigeria](https://productforums.google.com/forum/#!topic/gmail/3_wcd8tAqdc) ); if that is the case for you, reach out to People Ops to get the initial authentication code sent to a phone in  supported country, after which 2FA will work as normal.
   1. [ ] People Ops (Admin): Inform the manager that Google account has been created by mentioning them with a comment in the issue.
1. [ ] People Ops (Specialist): Invite GitLabber's GitLab.com account to the [gitlab-com group](https://gitlab.com/groups/gitlab-com/group_members) as a developer.
1. [ ] People Ops (Specialist): Add new team member to the [gitlab-org](https://gitlab.com/groups/gitlab-org/group_members) group on GitLab.com as a `Developer`.
1. [ ] 1Password
   1. [ ] New team member: Register on 1Password by clicking on the link in the ["Link to 1Password Sign-Up"](https://docs.google.com/document/d/15BdqpNhSi_5bc2b9KXo_w5f0gC-3gmuw-aIgzRcbnbk/edit) Google Doc and then ping @brittanyr to confirm your account. You can find this (and other) shared Google Docs by using [`Search for files shared with gitlab.com`](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
   1. [ ] New team member: Install the 1Password app on your computer, and link it to your team account, as described on the [security best practices page](https://about.gitlab.com/handbook/security/). Please let People Ops or your manager know if you need any assistance.
   1. [ ] New team member: Change your password on GitLab.com to use 1Password if you had an account before joining the company.
   1. [ ] New team member: If you created any accounts while onboarding before being added to 1Password, reset your passwords for them to use 1Password.
   1. [ ] New team member: Set up [secure passwords per the handbook](https://about.gitlab.com/handbook/security/).
1. [ ] Manager: In the "GitLab Email Forwarding" doc, suggest which email aliases the new team member should be added to, then ping one of the admins mentioned in the doc to help get it done.
1. [ ] Manager: Determine if new team member will need access to the `dev` server. If so, make [new dev.GitLab.org account](https://dev.gitlab.org/admin/users/new) and invite to the [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members) as a developer.
1. [ ] Manager: Let the new team member know their handle for dev.gitlab.org by mentioning it here.
1. [ ] Manager: Schedule a video call with new team member at the start of their first day.
1. [ ] Manager: Organize smooth onboarding with clear starting tasks / pathway for new team member.
1. [ ] Slack
   1. [ ] People Ops (Admin): Add to [Slack](https://gitlab.slack.com/admin).
   1. [ ] Manager: Please inform what [Slack groups](https://gitlab.slack.com/admin#user_groups) the new team member should be added to by adding this to the "GitLab Email Forwarding" document as a suggestion.
   1. [ ] New team member: Pick your [Slack username](https://gitlab.slack.com/account/settings#username) to be the same as your GitLab email handle, for consistency and ease of use.
   1. [ ] New team member: Fill in your [Slack profile](https://gitlab.slack.com/account/profile), including: What I Do (can be your job title or something informative about your role), Time Zone (useful for other GitLabbers to see when you're available), GitLab.com profile, and Job Description link.
   1. [ ] New team member: Introduce yourself in the Slack [#new_labbers](https://gitlab.slack.com/messages/new_labbers/) channel, where you can ask any questions you have and get to know other new team members!
1. [ ] People Ops Information
   1. [ ] New team member: Provide scan of photo ID to People Ops (used for verification, such as [I9 verification](https://www.uscis.gov/sites/default/files/files/form/i-9.pdf), and business travel purposes).
   1. [ ] New team member: Access BambooHR, set a secure password and fill out any missing details of your profile; specifically your address, phone number(s), banking information, ethnicity and emergency contact information. Make sure to add the country code to phone numbers (i.e. all numbers should start with `+`).
   1. [ ] People Ops (Admin): Move the applicant to the "Hired" bucket in Workable, this automagically creates their profile in BambooHR.
   1. [ ] People Ops (Admin): Find the profile on BambooHR and enable "self-service".
   1. [ ] People Ops (Admin): In BambooHR, input [relevant data](https://about.gitlab.com/handbook/people-operations/system-processes/#adding-a-new-team-member-to-bamboohr) into the GitLabbers profile. Be sure to add the personal and work email address before saving the newly entered information.
   1. [ ] People Ops (Admin): File signed contract with PIAA or NDA+IP agreements in BambooHR profile under "Documents"-->"Signed Contract/Offer Letter". Set the uploaded documents to "shared" so that the team member can also view them. For GitLab Inc. contractor agreements, also file the W9. If the contract has any exceptions to the IP agreement, check the applicable box under the Jobs tab.
   1. [ ] People Ops (Admin): File photo ID in BambooHR profile under "Documents"--> "Verification Docs".
   1. [ ] People Ops (Admin): List any stock options that were part of the offer in BambooHR profile under "Benefits"--> "Stock Options".
   1. [ ] People Ops (Specialsit): Add new team member to the comp calc. 
1. [ ] New team member: Read [Handbooks](https://about.gitlab.com/handbook/), read the relevant onboarding pages that are linked from there, and become comfortable with Git and the GitLab workflow.
1. [ ] Calendars & Agenda
   1. [ ] People Ops (Admin): Add entry to availability calendar so the team can see when new people are joining.
   1. [ ] People Ops (Admin): Give team member access to the GitLab availability calendar.
   1. [ ] People Ops (Admin): Add team member's birthday and work anniversary to the GitLab Birthdays calendar. Also, share this calendar with the new team member.
   1. [ ] People Ops (Admin): Add new team member to the next monthly [GitLab 101 call](https://about.gitlab.com/culture/gitlab-101/).
   1. [ ] People Ops (Admin): Invite the new team member to the monthly AMA call.
   1. [ ] People Ops (Admin): Invite to team meeting, and re-arrange the days on which GitLabbers speak in the Team Call Agenda.
   1. [ ] People Ops (Admin): If applicable, invite the new team member to the APAC team call.
   1. [ ] People Ops (Admin): Add entry to Team Call agenda to introduce the new team member, and make sure to include the new GitLabbers NAME as well as TITLE in the agenda item.
   1. [ ] People Ops (Admin): If in a technical role, add the new team member to the next Retro meeting and the Kickoff meeting that's scheduled and save for all events in the future.
   1. [ ] Manager: Add team member to weekly team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](/handbook/leadership/#1:1-meetings) with new team member.
1. [ ] People Ops (Admin): [Add blank entry to team page](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#blank-entry) (only the start date and position, use "logo-extra-whitespace.png" for the picture) so the team can see when new people are joining.
1. [ ] People Ops (Admin): Add to phishing testing platform.
1. [ ] People Ops (Admin): Check if new team member indicated who they were referred by (in bottom of application, in Workable). If no referral indicated, check with new team member by asking in this issue "@{new team member} please let People Ops know if you were referred by another GitLab team member; otherwise, we will assume you were not". If there is a referral indicated, ping @brittanyr in this issue with the referral name.
    1. [ ] People Ops (Admin): If the new team member was referred, add the referral bonus to BambooHR.
1. [ ] People Ops (Admin): Send brief welcome email to their personal address that directs the new team member to their GitLab email and their onboarding issue. Template text:

    > Welcome to GitLab, we're excited that you're joining our team!
    >
    > You should have received an invitation to your GitLab email account;
    > please let me know if that is not the case.
    >
    > Everything you need to get started is listed in your onboarding issue:
    > [Insert link]. People Ops will send you an
    > invitation to join the GitLab-com group in order to view the issue. If you already have
    > an account and want to separate work / personal handles on GitLab.com, you can make a
    > new account with your GitLab email address as the primary. Make sure that your
    > GitLab.com profile includes first and last name so e-mail notifications arrive correctly.

    > Check out Onboarding (https://about.gitlab.com/handbook/general-onboarding/) for
    > everything you need to know throughout the onboarding process. You might have
    > received invitations to create accounts for Slack, BambooHR, and other tools.
    > Before you do this, make sure you've set up a 1Password account as instructed in
    > your onboarding issue first. This will save you a lot of time later!
    >
    > We're looking forward to seeing you on our daily Team Call! The first time
    > that you join, please make sure that you connect at least 10 minutes
    > before the call in order to download Zoom and make sure that your camera and
    > microphone are working properly. We'd like you to introduce yourself to the team
    > (±1 minute), so please prepare some talking points for yourself. Please make sure to
    > tell us about where you were before GitLab, why you wanted to join our team,
    > and what you like to do in your spare time. Check out the Team Call page of our
    > handbook to learn more: https://about.gitlab.com/handbook/communication/#team-call.

    > Please let People Ops (cc'd here) know if there are any necessary supplies
    > or equipment that you need. You can also reference the company handbook
    > (https://about.gitlab.com/handbook/spending-company-money) in this matter.

1. [ ] People Ops (Admin): Inform the manager about the hire by announcing in general chat channel 'X has joined GitLab as (title)!'
1. [ ] People Ops (Admin): Create a row for the new user on the [1Password spreadsheet](https://docs.google.com/spreadsheets/d/1dSlIRbD1b8XisIBbANDK5NGe55LvVSTsWM7aHcWCOVU/edit#gid=0).

### ON YOUR FIRST DAY

#### For employees in the USA only (GitLab Inc)

1. [ ] People Ops (Specialist): Gather relevant information from new team member to enter them into the TriNet system.
1. [ ] People Ops (Specialist): Complete and submit an online Add New Hire Form
(TriNet Passport=>My Workplace=> Add New Hire/Rehire). This will generate the
welcome email to the employee at their work email on their first date of hire.
1. [ ] People Ops (Specialist): If new team member is an hourly employee, create a timesheet, share with Finance and the new team member. Also, send an email to Finance to let them know that an hourly employee has started.
1. [ ] New employee: Complete [New Hire TriNet Passport 7 Steps Guide](https://docs.google.com/a/gitlab.com/document/d/1CFIyByd1Kgmz5353_aASVI1D8QTyJ2Uy60ziQHEPTUI/edit?usp=sharing). The I-9 portion of this must be completed within the first two days of hire. Note- this is critical so you must contact People Ops if you have difficulty with this form. (TriNet takes 24-48 hours to process new GitLabbers.)
1. [ ] People Ops (Specialist): Complete the employer portion of the I9 in TriNet within three days of hire.
1. [ ] New employee: Read through the [New Hire Benefits Guide](https://drive.google.com/a/gitlab.com/file/d/0B0dixQ9qzgilNlN0MnNFS29xWnB2SjNWVUk3dUV2aWlhejVR/view?usp=sharing). This will go over medical, dental, vision, and voluntary benefits. Note - If you have any questions or need help within the TriNet system please contact the Employee Solution Center at 800-638-0461 or email them at employees@trinet.com.

#### For employees in the Netherlands only (GitLab BV)

1. [ ] People Ops: Fill in this payroll information [form](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSekAouLh-DQsDXVho4TYL62CsBK8Pj0NhfI--npa3L1nF8IqA/viewform) using the data the team member entered into BambooHR and send the HR Savvy.
This info is needed to get the team member's profile ready with Savvy HR in order to get pay slips and other information. People Ops will file in BambooHR.

#### For employees in the UK only (GitLab Ltd)

1. [ ] People Ops (Specialist): Using the template, send new team member the UK payroll form and New Starter Checklist (only if the new team member does not have a P45) to complete (this can be found in the Google Drive/UK Documents folder). Fill in as much information as possible from BambooHR and stage in HelloSign for signature. Cc People Ops and Vistra (email address is located on the Payroll form).
1. [ ] New Team Member: Once you have received the payroll form please fill in the missing details and sign. This form will then be sent electronically to GitLab's payroll provider Vistra. If you have a P45 from your previous company please post this to Vistra at the address at the bottom of the payroll form. If you don't have a P45 then you must complete the New Starter Checklist and post this to Vistra.
1. [ ] People Ops (Specialist): Once the payroll form has been completed and signed by the new team member file the document in BambooHR under Documents/Payroll Forms.
1. [ ] New Team Member: Read through the [AXA PPP Brochure](https://drive.google.com/a/gitlab.com/file/d/0Bwy71gCp1WgtUXcxeFBaM0MyT00/view?usp=sharing) and let People Ops know if you would like to join the medical insurance scheme. This does not currently include dental or optical care. Please also note that this is a P11d taxable benefit.

#### For employees in Belgium only (GitLab BV)

1. [ ] New Team Member: To ensure that we have all of your details ready for payroll processing please complete the following information on BambooHR:
     1. [ ] Full name
     1. [ ] Date of birth
     1. [ ] Address
     1  [ ] National Number
     1. [ ] Bank details
1. [ ] People Ops (Specialist): Reach out to new team member and ask them to confirm their marital status and number of children. This is required for tax purposes.
1. [ ] People Ops (Specialist): Once all the information has been entered by the new team member, send this to B&F Consult CVBA by email along with a:
     1. [ ] Scanned copy of employment contract
     1. [ ] ID card
     1. [ ] Email address
     1. [ ] Salary

#### For employees in India only (Lyra)

1. [ ] New team member: Read through the [India Specific Benefits](https://about.gitlab.com/handbook/benefits/#india-specific-benefits). This will explain what is available. If you have any questions please contact Lyra HR at +91 80 40408181 or email them at hr@lyrainfo.com. Lyra HR will also reach out to you in the first week of starting at GitLab to complete their onboarding documents for payroll (if this has not already been done during the contract signing stage).

#### All GitLabbers

1. [ ] New team member: Read the [security best practices](https://about.gitlab.com/handbook/security/), and please ask questions to make sure it is all clear to you.
1. [ ] New team member: Enable 2FA on your GitLab email account (this should have been an option to do when you first received the invitation to the account).
1. [ ] New team member: If you authenticated your GitLab.com account with Google, GitHub, etc. you should either disconnect them or make sure they use two-factor authentication.
1. [ ] New team member: Enable [two-factor authentication](http://docs.gitlab.com/ee/profile/two_factor_authentication.html) on your GitLab.com account.
1. [ ] New team member: Fill in your [GitLab.com profile](https://gitlab.com/profile), including: Organization = "GitLab", Bio = "(title) at GitLab".
1. [ ] New team member: Make sure your notifications for GitLab.com aren't sent to a personal email. Send them to your `@gitlab.com` email.
1. [ ] 1Password
    1. [ ] New team member: Once you are registered on 1Password, post a comment below to ask the manager to add you to the 1Password groups required for your role.
    1. [ ] Manager: Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping @brittanyr with which vaults the new team member should be added to.
1. [ ] New team member: Set up [Calendly](https://about.gitlab.com/handbook/tools-and-tips/#calendly), and add your Calendly link to your Slack profile.
1. [ ] New team member: Set up [Zoom account](https://about.gitlab.com/handbook/tools-and-tips/#zoom) with your GitLab.com email address, and fill in your [profile](https://zoom.us/profile): Name and Profile Picture.
1. [ ] New team member: Send an email to People Ops with the following information for your business cards: Name (how you would like it printed), Phone Number, Twitter handle, City, State (if applicable), and country. The city can be your metro area if different from your address. 
1. [ ] New team member: If you purchased and expensed your laptop, send an email to People Ops with the serial code of your GitLab laptop.

### WITHIN FIRST WEEK OF STARTING

1. [ ] People Ops (Admin): Invite the new team member to [Printfection](https://www.printfection.com/) to order swag.
1. [ ] People Ops (Admin): Send brief welcome email to their GitLab email address with the following message regarding Swag credit:

    > Happy first day at GitLab! We're excited that have you on the team!
    > Please use this link [link] to access our swag store and order some GitLab swag to celebrate your arrival!
    > Please see the "Tech Tanuki" [size chart](https://www.dropbox.com/s/mk14v344aoygsc4/Screenshot%202017-05-30%2010.04.57.png?dl=0) and all other shirts' [size chart](https://www.dropbox.com/s/87209zmn65r7mtq/Screenshot%202017-05-30%2009.13.23.png?dl=0).

1. [ ] People Ops (Admin): Remove user from the [Google 'New Users' group](https://admin.google.com/gitlab.com/AdminHome?groupId=new-users@gitlab.com&chromeless=1#OGX:Group). This causes 2FA enforcement for the user.
1. [ ] People Ops (Admin): [Invite to autoconnect](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#add-beamy) on [Beamy](https://app.suitabletech.com/).
1. [ ] New team member: Use [Beamy](https://app.suitabletech.com/) to explore the office, following the [Beamy Guidelines](https://about.gitlab.com/handbook/communication/#beamy-guidelines). Please do not log in outside of available hours because you might wake Sid up - Beamy is available 8am-6pm PST ([check the current time](https://time.is/San_Francisco)). Please park it back on the charger when you're done!
1. [ ] People Ops (Specialist): Order [business cards](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#b-cards) for new team member.
1. [ ] People Ops (Specialist): Send the new team member their Traveler Insurance ID card with a [link the to the handbook](https://about.gitlab.com/handbook/benefits/#general-benefits).
1. [ ] People Ops (Admin): Add team member to [Expensify](https://www.expensify.com/signin) (if employee). See SOP for more [instructions](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#add-expensify).
1. [ ] People Ops (Admin): Add new team member to the info sheets of the next Summit if applies.
1. [ ] New team member: Check if there is a coming [Summit](https://gitlab.com/summits/) and save the date!
1. [ ] People Ops (Admin): [Create a profile](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#add-egencia) on [Egencia](https://www.egencia.com/auth/v1/login) for new team member.
1. [ ] New team member: Schedule 10 calls of 30 mins with 10 different colleagues to get to know your team. You can also schedule more calls, or ask for help on the #questions channel in slack.
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
1. [ ] New team member: Link your GitLab email address to an easily recognizable photo of yourself on [Gravatar](https://en.gravatar.com/). It is company policy to use a photo, and not an avatar, a stock photo, or something with sunglasses for your GitLab accounts, as we have a lot of GitLabbers and our brains are comfortable with recognizing people; let's use them.
1. [ ] New team member: Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).
1. [ ] New team member: [Add yourself](https://about.gitlab.com/handbook/general-onboarding/#add-yourself-to-the-team-page) to the [team page](https://about.gitlab.com/team/) with an easily recognizable photo. Assign the merge request to your manager.
1. [ ] New team member: Add yourself to the "New Hire Tweets" Google [doc](https://docs.google.com/document/d/1NmrgUXnigXXptEnxjBTI9sBWKyWyynBP_8K-S0fYC1w/edit). Add the template text for the tweet that announces your entry to the team: "Very happy to announce that [NAME] has joined #TeamGitLab! Welcome [TWITTER HANDLE]! https://about.gitlab.com/team/". Emily v.H. will schedule a tweet about the new hire when the addition to the team page has been merged.
1. [ ] New team member: Add yourself to the [public map](https://sundial.teleport.org/public/groups/Y3IahPR5vYjBpMyU2kZj) of everyone's location via [Sundial](https://docs.google.com/a/gitlab.com/document/d/1U0ZYlKgX_VZVCKUozRYehRSiNquzIN1hg8B2RP19QCw/edit?usp=sharing).
1. [ ] New team member: Encrypt the home folder of your hard drive (see the [security best practices](https://about.gitlab.com/handbook/security) for help).
1. [ ] New team member: Leave a comment in this issue with a screenshot verifying that your hard drive is encrypted (`Command` + `Shift` + `3` on Mac).
1. [ ] New team member: Make an improvement to the handbook (something that you wished was there), assign the merge request (MR) to your manager and link the MR url in this onboarding issue.
1. [ ] New team member: Follow the Git and GitLab course on [Platzi](https://courses.platzi.com/classes/git-gitlab/).
1. [ ] New team member: Become familiar with how GitLab works by learning our [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html).
1. [ ] New team member: Reach out to your onboarding buddy, or any other GitLabber to show you how to do some advanced git tasks locally. For example, configure and/or solve a merge conflict.
1. [ ] New team member: Set-up and familiarize yourself with our apps: [Gmail](https://mail.google.com/), [Google Calendar](https://www.google.com/calendar/), [Slack](https://gitlab.slack.com/messages/general/) and [Google Drive](https://www.google.com/drive/) where you can [download](https://tools.google.com/dlpage/drive/index.html?hl=en) to work offline. Be aware your Google Calendar (tied to your GitLab Google account) is internally viewable by default, you might want to [change this](https://support.google.com/calendar/answer/34580?co=GENIE.Platform%3DDesktop&hl=en) in your calendar settings.
1. [ ] New team member: Take a look at the "Reference Customers" Google doc to familiarize yourself with which customer information should be kept confidential within the company. You can find it by logging into your Google Drive account and searching for "Reference Customers".
1. [ ] New team member: Check out the [courses page](https://about.gitlab.com/courses/) which has additional resources, they are organized by department so you can choose the most relevant courses based on your role.
1. [ ] Manager: Set new GitLabbers' project-level permissions as-needed.


### FOR ENGINEERING ONLY (Devs, PEs, SEs)

1. [ ] Manager: Add GitLabbers' GitHub.com account to the [gitlabhq organization](https://github.com/orgs/gitlabhq/people) (if applicable).
1. [ ] Manager: Add team member to the backend group call (if applicable).
1. [ ] Manager: (For Production GitLabbers) Create a new [Production Engineering onboarding checklist](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/onboarding_template.md).
1. [ ] People Ops: (For Production GitLabbers) [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] New team member: (For Developer GitLabbers) Read the [developer documentation](http://docs.gitlab.com/ce/development/README.html).
1. [ ] Manager: Provide access to PagerDuty if applicable.
1. [ ] Manager: (For Build Engineers) Add new team member as a member to the [GitLab Dev Digital Ocean account](https://cloud.digitalocean.com/settings/team).
1. [ ] Context & Comfort with GitLab.com's production environment:
  1. [ ] New team member: review issues labeled as `outage` in the [infrastructure issue tracker](https://gitlab.com/gitlab-com/infrastructure/issues/).
  1. [ ] New team member: read about the [production architecture](https://about.gitlab.com/handbook/infrastructure/production-architecture) and [monitoring](https://about.gitlab.com/handbook/infrastructure/monitoring).
  1. [ ] New team member: make a change in production (i.e. on GitLab.com). The idea is to get comfortable with the production architecture and environment but not to spend more than 0.5-1 day on this onboarding task. Examples can include building monitoring and [alerting](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=alert) for a feature or service; building an automation in [gitlab-cog](https://gitlab.com/gitlab-cog); etc.

#### FOR SUPPORT ENGINEERING ONLY

1. [ ] Manager: Open a new [support onboarding boot camp issue](https://gitlab.com/gitlab-com/support/issues) using the support [onboarding checklist](https://about.gitlab.com/handbook/support/onboarding/checklist), and provide the link in a comment below this onboarding checklist.
1. [ ] Manager: Provide access to [hackerone.com](https://www.hackerone.com/).
1. [ ] Zendesk:
   1. [ ] Manager: [Add new team member](https://support.zendesk.com/hc/en-us/articles/203661986-Adding-end-users-agents-and-administrators#topic_h43_2k2_yg) as an agent in [GitLab ZenDesk](https://gitlab.zendesk.com); you may need to [purchase a new license](https://about.gitlab.com/handbook/support/workflows/zendesk/#adding--removing-agents-in-zendesk)
   1. [ ] Manager: Add agent to required [support groups](https://support.zendesk.com/hc/en-us/articles/203661766-About-organizations-and-groups) in [GitLab ZenDesk](https://gitlab.zendesk.com).
1. [ ] Community Forum:
   1. [ ] New team member: Create new account for the [GitLab community forum](https://forum.gitlab.com/) using the sign in with GitLab option and mention the username used.
   1. [ ] Manager: Add the 'moderator' flag to the new team member's user on the [GitLab community forum](https://forum.gitlab.com/).
1. [ ] Manager: Add new team member as a member to the GitLab Dev Digital Ocean account https://cloud.digitalocean.com/settings/team
1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] New team member: Register on timezone.io by clicking on the link in the **"Link to Timezone.io Sign-Up"** - You can find this (and other) shared Google Docs by using [`Search for files shared with gitlab.com`](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.

#### FOR COMMUNITY ADVOCATES ONLY

1. [ ] Manager: Open a new [support onboarding boot camp issue](https://gitlab.com/gitlab-com/support/issues) using the support [onboarding checklist](https://about.gitlab.com/handbook/support/onboarding/checklist), and provide the link in a comment below this onboarding checklist.
1. [ ] Zendesk:
   1. [ ] Manager: [Add new team member](https://support.zendesk.com/hc/en-us/articles/203661986-Adding-end-users-agents-and-administrators#topic_h43_2k2_yg) as an agent in [GitLab ZenDesk](https://gitlab.zendesk.com); you may need to [purchase a new license](https://about.gitlab.com/handbook/support/workflows/zendesk/zendesk_tips.html#adding--removing-agents-in-zendesk).
   1. [ ] Manager: Add agent to required [support groups](https://support.zendesk.com/hc/en-us/articles/203661766-About-organizations-and-groups) in [GitLab ZenDesk](https://gitlab.zendesk.com).
1. [ ] Community Forum:
   1. [ ] New team member: Create new account for the [GitLab community forum](https://forum.gitlab.com/) using the sign in with GitLab option and mention the username used.
   1. [ ] Manager: Add new team member to "moderators" group on the [GitLab community forum](https://forum.gitlab.com/).
1. [ ] TweetDeck:
   1. [ ] Manager: Add team member to [TweetDeck](https://tweetdeck.twitter.com/).
   1. [ ] New team member: Enable "Confirmation Step" for all GitLab accounts to prevent accidental tweeting.
1. [ ] New team member: View [Support Handbook](https://about.gitlab.com/handbook/support/) since many tools/processes overlap.
1. [ ] New team member: Read through and familiarize yourself with the [Community Advocacy](https://about.gitlab.com/handbook/marketing/developer-relations/community-advocacy/) page.
1. [ ] New team member: Create an account on [Stack Overflow](http://stackoverflow.com/) if you don't already have one. Start by answering a few simple questions in an area you're familiar with (a language, web framework, development platform, API, etc.), or in the GitLab tag if you feel comfortable. The goal is to get enough "Reputation" and have access to a few more features.
1. [ ] Disqus:
   1. [ ] New team member: Create a Disqus account, connect it to your `@gitlab.com` Google account.
   1. [ ] Manager: Give the user the ability to moderate Disqus comments on the blog.
1. [ ] New team member: Create a [Hacker News](https://news.ycombinator.com/news) account if you don't have one already, make sure to specify in your user bio that you're a Community Advocate at GitLab, Hacker News requires that we be transparent about any conflicts of interest.

#### FOR UX DESIGNERS, FRONTEND DEVS, AND DESIGNERS ONLY

1. [ ] New team member: (For Designers) Get the [Pixelmator app](https://itunes.apple.com/us/app/pixelmator/id407963104?mt=12&ign-mpt=uo%3D4) and expense it.
1. [ ] New team member: (For Frontend Developers) If you do not have access to Windows on your computer, please download a [Windows VM](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/). We recommend using [VMWare Fusion](http://www.vmware.com/products/fusion.html) or [Parallels Desktop](http://www.parallels.com/products/desktop/) for Window virtualization. You may expense the purchase to GitLab or ask People Ops to get you a copy.
1. [ ] New team member: Join the [#ux](https://gitlab.slack.com/messages/ux/) channel on Slack.
1. [ ] People Ops (Admin): (For Designers) provide [SketchApp](http://www.sketchapp.com/) license by [ordering online](https://sites.fastspring.com/bohemian/order/contents), and forwarding the email with the license key to the new team member.
1. [ ] People Ops (Admin): (For Designers only, not UX Designers) provide access to [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html) using the shared credential in the Secretarial vault. If a UX Designer feels that they need CC Photoshop and/or CC Illustrator they can request it.
1. [ ] Manager: (For UX Designers) Give new team member `Master` access to the [gitlab-design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: (For UX Designers) Add new team member to the [GitLab Dribbble team](https://dribbble.com/gitlab).
1. [ ] People Ops (Admin): (For UX Designers) Add new team member to the `@uxers` User Group on Slack.

### FOR MARKETING ONLY

1. [ ] People Ops: Ping Amanda in the issue to add to Tweetdeck, unless position title is BDR.

### FOR SALES, CUSTOMER SUCCESS AND FINANCE ONLY

1. [ ] Finance: Add to Comerica (as user or viewer only if in Finance).
1. [ ] Finance: Add to [QuickBooks users](https://about.gitlab.com/handbook/hiring/) (Finance only).
1. [ ] Manager: Create Issue for [Sales Bootcamp](https://about.gitlab.com/handbook/sales-onboarding/).  Naming convention for issue is "New hires name - Sales Bootcamp".  Due date is 30 days after start date.
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] People Ops: [Upgrade new team member's Zoom account](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro) to Pro.
1. [ ] Sales Operations (Francis): Add to  [Salesforce], [Sertifi].
1. [ ] Sales Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.
1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me), familiarize yourself with:
  1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)
  1. Competition https://about.gitlab.com/comparison/
1. [ ] New team member: Familiarize yourself with the [Support](https://about.gitlab.com/handbook/support/) process.

### FOR PEOPLE OPS ONLY

* [ ] Manager: Add team member to applicable email addresses.
* [ ] People Ops (Specialist): Add team member to BambooHR and Workable as an admin (if applicable).
* [ ] Manager: Add team member to TriNet and HR Savvy as an admin (if applicable).
* [ ] People Ops (Admin): Invite team member to People Ops Confidential channel in Slack.

### FOR CORE TEAM MEMBERS ONLY

1. [ ] People Ops (Specialist): Add email address to the [mailing list](https://groups.google.com/forum/#!forum/gitlab-core).
1. [ ] People Ops (Specialist): Have member sign NDA.
1. [ ] People Ops (Specialist): Add member to #core and #development in Slack.
1. [ ] People Ops (Specialist): Give member developer access to gitlab-org.
1. [ ] People Ops (Specialist): Change to core team member in BambooHR.
1. [ ] Core Team Member: Add yourself to the [Core Team](https://about.gitlab.com/core-team/) page.
